const dino = document.getElementById("dino");
const cactus = document.getElementById("cactus");
const score = document.getElementById("score");
const seagull = document.getElementById("seagull");

function jump() {
    dino.classList.add("jump-animation");
    setTimeout(() =>
        dino.classList.remove("jump-animation"), 500);
}

document.addEventListener("keypress", () => {
    if (!dino.classList.contains("jump-animation")) {
        jump();
    }
})
setInterval(() => {

    const dinoTop = parseInt(window.getComputedStyle(dino)
        .getPropertyValue("top"));
    const cactusleft = parseInt(window.getComputedStyle(cactus)
        .getPropertyValue("left"));
    const seagullleft = parseInt(window.getComputedStyle(seagull)
        .getPropertyValue("left"));
    score.innerText++;

    if (cactusleft < 0) {
        cactus.style.display = "none";
    } else {
        cactus.style.display = ""
    }
    if (seagullleft < 0) {
        seagull.style.display = "none";
    } else {
        seagull.style.display = ""
    }

    if ((cactusleft < 50 && cactusleft > 0 && dinoTop > 150) || (seagullleft < 50 && seagullleft > 0 && dinoTop > 150)) {
        alert("You got a score of: " + score.innerText +
            "\n\nPlay again?");
        location.reload();
    }
}, 50);